-- Set leader key
vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

local opts = { noremap = true, silent = true }
local kms= vim.keymap.set

-- Disable the spacebar key's default behavior in Normal and Visual modes
kms({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })

-- save file
kms('n', '<C-s>', '<cmd> w <CR>', opts)

-- save file without auto-formatting
kms('n', '<leader>sn', '<cmd>noautocmd w <CR>', opts)

-- quit file
kms('n', '<leader>bx', '<cmd> q <CR>', opts)

-- delete char-r without copyng
kms('n', 'x', '"_x', opts)

-- Vertical scroll an center
kms('n', '<C-d>', '<C-d>zz', opts)
kms('n', '<C-u>', '<C-u>zz', opts)

-- Find and center
kms('n', 'n', 'nzzzv', opts)
kms('n', 'N', 'Nzzzv', opts)

-- Buffers
kms('n', 'H', '<cmd> bprevious <CR>', opts)
kms('n', 'L', '<cmd> bnext <CR>', opts)
kms('n', 'C-q', '<cmd> bdelete <CR>', opts)
kms('n', '<leader>bn', '<cmd> enew <CR>', opts)

-- Windows
kms('n', '<leader>wv', '<C-w>v', opts)
kms('n', '<leader>wh', '<C-w>s', opts)
kms('n', '<C-h>', '<C-w>h', opts)
kms('n', '<C-j>', '<C-w>j', opts)
kms('n', '<C-k>', '<C-w>k', opts)
kms('n', '<C-l>', '<C-w>l', opts)

-- Toggles
kms('n', '<leader>tw', '<cmd>set wrap!<CR>', opts)

-- Stay in indent mode
kms('v', '<', '<gv', opts)
kms('v', '>', '>gv', opts)

-- Copy and paste
kms('v', 'p', '"_dP', opts) -- keep last yanked when pasting

-- Diagnostic
kms('n', '[d', vim.diagnostic.goto_prev, { desc = 'previous diag message' })
kms('n', ']d', vim.diagnostic.goto_next, { desc = 'next diag message' })
kms('n', '<leader>d', vim.diagnostic.open_float, { desc = 'Open diag message' })
kms('n', '<leader>q', vim.diagnostic.setloclist, { desc = 'Open diag list' })
