-- Global

-- Command

-- Options
vim.wo.number = true
vim.wo.signcolumn = yes

vim.opt.autoindent = true
vim.opt.clipboard = "unnamedplus"
vim.opt.cursorline = true
vim.opt.expandtab = true
vim.opt.hlsearch = false
vim.opt.ignorecase = true
vim.opt.linebreak = true
vim.opt.mouse = "a"
vim.opt.numberwidth = 4
vim.opt.relativenumber = true
vim.opt.scrolloff = 4
vim.opt.shiftwidth = 4
vim.opt.showmode = false
vim.opt.smartcase = true
vim.opt.smartindent = true
vim.opt.softtabstop = 4
vim.opt.splitbelow = true
vim.opt.splitright = true
vim.opt.swapfile = false
vim.opt.tabstop = 4
vim.opt.termguicolors = true
vim.opt.whichwrap = 'bs<>[]hl'
vim.opt.wrap = true
vim.opt.showtabline = 2
vim.opt.backspace = 'indent,eol,start'
vim.opt.pumheight = 10
vim.opt.conceallevel = 0 -- TODO check which better
vim.opt.fileencoding = 'utf-8'
vim.opt.cmdheight = 1
