-- Colorscheme
vim.g.background = "light"

-- Highlights
vim.api.nvim_set_hl(0, "Comment", { italic = true, fg = "#aaaaaa" })

vim.api.nvim_set_hl(0, "Keyword", { bold = true })
vim.api.nvim_set_hl(0, "Function", { fg = "#ff8800" })
