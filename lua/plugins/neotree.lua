return {
    'nvim-neo-tree/neo-tree.nvim',
    version = '*',
    dependencies = {
        'nvim-lua/plenary.nvim',
        'nvim-tree/nvim-web-devicons', -- not strictly required, but recommended
        'MunifTanjim/nui.nvim',
    },
    cmd = 'Neotree',
    keys = {
    { '\\', ':Neotree reveal<CR>', desc = 'NeoTree reveal', silent = true },
    },
    opts = {
        filesystem = {
            window = {
                width = 30,
                mappings = {
                ['\\'] = 'close_window',
                ['l'] = 'open',
                ['h'] = 'close_node',
                },
            },
    filtered_items = {
        hide_dotfiles = false,
    },
    follow_current_file = {
        enabled = true,
        leave_dirs_open = false,
    },
        },
    
    default_component_configs = {
	    name = {
		    use_git_status_colors = false},
    indent = {
        with_expanders = true, -- if nil and file nesting is enabled, will enable expanders
        expander_collapsed = "",
        expander_expanded = "",
        expander_highlight = "NeoTreeExpander",
    },
  },
  },
}
